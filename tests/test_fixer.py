#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A series of Python module source-code that will get newlines added to them."""

import textwrap
import unittest

import line_manager


class Common(unittest.TestCase):
    @staticmethod
    def _fix(code):
        """str: Get the fixed code and return it."""
        return "\n".join(line_manager.get_corrected_code(code))


class Scenarios(Common):
    """A class that contains a variety of source-code and their expected "fixed" states."""

    def test_single_control_statement(self):
        """Test that a function that contains a single flow-statement does not add a newline."""
        code = textwrap.dedent(
            """
            def another():
                return 'asdf'
            """
        ).strip()

        expected = code

        self.assertEqual(expected, self._fix(code))

    def test_nested_function(self):
        """Test that a function with multiple flow-statements adds a newline."""
        code = textwrap.dedent(
            '''
            def foo():
                def another():
                    """asdfasdfsfd"""
                    if 'XXX': pass
                    return 'asdf'
                raise ValueError('asdf')
            '''
        ).strip()

        expected = textwrap.dedent(
            '''
            def foo():
                def another():
                    """asdfasdfsfd"""
                    if 'XXX': pass

                    return 'asdf'

                raise ValueError('asdf')
            '''
        ).strip()

        self.assertEqual(expected, self._fix(code))

    def test_complex_001(self):
        """Test a more complicated parsing scenario. Add newlines wherever needed."""
        code = textwrap.dedent(
            """
            def function():
                def inner():
                    if 'asfd': pass
                    # asdfasdf
                    # asdfasfdad
                    yield 0
                thing = 'asdf'
                # asdfasfd
                yield 'asfd'
                while False:
                    pass
            """
        ).strip()

        expected = textwrap.dedent(
            """
            def function():
                def inner():
                    if 'asfd': pass

                    # asdfasdf
                    # asdfasfdad
                    yield 0
                thing = 'asdf'

                # asdfasfd
                yield 'asfd'

                while False:
                    pass
            """
        ).strip()

        self.assertEqual(expected, self._fix(code))

    def test_with(self):
        code = textwrap.dedent(
            """
            def another():
                foo = []
                with Blah():
                    pass
            """
        ).strip()

        expected = textwrap.dedent(
            """
            def another():
                foo = []

                with Blah():
                    pass
            """
        ).strip()

        self.assertEqual(expected, self._fix(code))


class Bugs(Common):
    def test_escaped_string(self):
        code = textwrap.dedent(
            """
            inf_msg = "Foo " \
                      "Bar"
            """
        ).strip()

        expected = code

        self.assertEqual(expected, self._fix(code))

    def test_elif(self):
        code = textwrap.dedent(
            """
            if False:
                return 8
            elif False:
                return 'asdf'
            """
        ).strip()

        expected = code

        self.assertEqual(expected, self._fix(code))

    def test_try_except_001(self):
        code = textwrap.dedent(
            """
            try:
                for infnode in decnode.infer():
                    result.add(infnode.qname())
            except exceptions.InferenceError:
                continue
            """
        ).strip()

        expected = code

        self.assertEqual(expected, self._fix(code))

    def test_try_except_002(self):
        code = textwrap.dedent(
            """
            try:
                for infnode in decnode.infer():
                    result.add(infnode.qname())
            except exceptions.InferenceError:
                pass
            finally:
                continue
            """
        ).strip()

        expected = code

        self.assertEqual(expected, self._fix(code))

    def test_try_except_003(self):
        code = textwrap.dedent(
            """
            try:
                for infnode in decnode.infer():
                    result.add(infnode.qname())
            except exceptions.InferenceError:
                pass
            finally:
                if thing:
                    pass
                continue
            """
        ).strip()

        expected = textwrap.dedent(
            """
            try:
                for infnode in decnode.infer():
                    result.add(infnode.qname())
            except exceptions.InferenceError:
                pass
            finally:
                if thing:
                    pass

                continue
            """
        ).strip()

        self.assertEqual(expected, self._fix(code))

    def test_while_after_statement(self):
        code = textwrap.dedent(
            """
            def _c3_merge(sequences, cls, context):
                result = []
                while True:
                    sequences = []
                    if not sequences:
                        return result
            """
        ).strip()

        expected = textwrap.dedent(
            """
            def _c3_merge(sequences, cls, context):
                result = []

                while True:
                    sequences = []

                    if not sequences:
                        return result
            """
        ).strip()

        self.assertEqual(expected, self._fix(code))


class ControlExpressions(Common):
    """A series of tests for control flow statements that contain expressions."""

    def test_expression_001(self):
        """Test that an if condition with an expression works correctly."""
        code = textwrap.dedent(
            """
            if some_function('asdf'):
                return 8
            """
        ).strip()

        expected = code

        self.assertEqual(expected, self._fix(code))

    def test_expression_002(self):
        """Test that an if condition with an expression works correctly."""
        code = textwrap.dedent(
            """
            def foo():
                if some_function('asdf'):
                    return 8
            """
        ).strip()

        expected = code

        self.assertEqual(expected, self._fix(code))

    def test_expression_003(self):
        """Test that an if condition with an expression works correctly."""
        code = textwrap.dedent(
            """
            def foo():
                def inner():
                    yield
                if some_function('asdf'):
                    return 8
            """
        ).strip()

        expected = textwrap.dedent(
            """
            def foo():
                def inner():
                    yield

                if some_function('asdf'):
                    return 8
            """
        ).strip()

        self.assertEqual(expected, self._fix(code))
