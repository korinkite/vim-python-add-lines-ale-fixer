#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A module that adds newlines in Python source code, wherever needed."""

# IMPORT THIRD-PARTY LIBRARIES
from parso.python import tree
from parso import normalizer
import parso


class NodeWrapper(object):
    """A class that forces a leading newline to whatever node it is given.

    This class is meant to be swap-able with a parso Python node. But
    when the node's code is retrieved, this class will add a newline.

    """

    def __init__(self, node):
        """Store some node that this instance will add a newline to.

        Args:
            node (`parse.tree.LeafOrNode`):
                Some object that will be wrapped with newlines.

        """
        super(NodeWrapper, self).__init__()
        self.node = node

    def get_code(self):
        """str: Add a newline to the given node's code."""
        code = self.node.get_code()

        if not code.startswith("\n"):
            code = "\n" + code

        return code

    def __getattr__(self, name):
        """Send properties / method calls that this object doesn't define to the wrapped node.

        Args:
            name (str): The name of the property or method to query.

        Returns:
            The queried object.

        """
        return getattr(self.node, name)


class FlowControlVisitor(normalizer.Normalizer):
    """A class that specializes in finding parso nodes that need newlines."""

    _control_types = (
        tree.ForStmt,
        tree.IfStmt,
        tree.ReturnStmt,
        tree.TryStmt,
        tree.WhileStmt,
        tree.WithStmt,
        tree.YieldExpr,
    )

    def __init__(self, grammar, config):
        """Keep a variable to store all found flow-control statements."""
        super(FlowControlVisitor, self).__init__(grammar, config)
        self._flow_controls = set()

    @classmethod
    def _is_control_node(cls, node):
        """bool: If the node represents something that may need a newline"""
        return isinstance(node, cls._control_types) or cls._is_generic_node(node)

    @staticmethod
    def _is_generic_node(node):
        """bool: Check if the node doesn't have a special class but still may need a newline."""
        if isinstance(node, tree.Keyword) and node.value in (
            "break",
            "continue",
            "return",
            "yield",
        ):
            return True
        elif isinstance(node, tree.KeywordStatement) and node.keyword == "raise":
            return True

        return False

    @staticmethod
    def _is_only_child(node):
        """bool: Return False if `node` is the only node in its parent."""
        def _is_part_of_compound(node):
            return node.type == 'suite'

        def _is_or_partly_special(node):
            return _is_special(node) or _is_part_of_compound(node)

        parent = _get_parent(node, _is_or_partly_special)

        if not parent:
            return False

        statements = [
            sibling
            for sibling in parent.children
            if not isinstance(sibling, tree.Newline)
        ]

        return len(statements) < 2

    @staticmethod
    def _ignore_needs_newline(node):
        """bool: Recommend not adding a newline if `node` is the first non-whitespace statement."""
        if not _force_pep8_conventions():
            return False

        return _has_important_previous_sibling(node)

    @classmethod
    def _needs_newline(cls, node):
        """bool: If the given node needs a newline.

        Args:
            node (`parse.tree.LeafOrNode`): The node that will be checked.

        Returns:
            bool: If the node needs to be wrapped with a leading newline.

        """
        if not cls._is_control_node(node):
            return False

        if cls._is_only_child(node):
            # If a control statement is by itself, chances are it is in a function like
            #
            # def foo():
            #     return 'bar'
            #
            # We don't want to add a newline in this case because that'd
            # go against PEP8 / pylint.
            #
            return False

        return cls._ignore_needs_newline(node)

    def visit(self, node):
        """Find evey node that needs newlines and recursively check the node's children, too."""
        if self._needs_newline(node):
            self._flow_controls.add(node)

        return super(FlowControlVisitor, self).visit(node)

    def get_flow_controls(self):
        """set[parse.tree.LeafOrNode`]: Get every important node that was visited."""
        return self._flow_controls


def _has_important_previous_sibling(node):
    """bool: Check if `node` has a sibling node that isn't just whitespace."""
    if not node.parent:
        return False

    leaf = node.get_previous_sibling()

    while leaf:
        if _is_special(leaf):
            return True

        leaf = leaf.get_previous_sibling()

    if _is_special(node.parent):
        return False

    if isinstance(node.parent, tree.Function):
        return False

    return _has_important_previous_sibling(node.parent)


def _is_special(node):
    """bool: Check if `node` is an actual Python statement or compound that contains a statement."""
    if isinstance(node, (tree.Newline, tree.String)):
        return False

    if isinstance(
        node, tree.PythonNode
    ):  # Is a compound node that needs to be traversed
        return any(_is_special(child) for child in node.children)

    return isinstance(node, (tree.Scope, tree.Flow, tree.ExprStmt))


def _force_pep8_conventions():
    """bool: Avoid adding newlines if doing so breaks PEP8 conventions."""
    try:
        import vim
    except ImportError:
        return True

    try:
        return vim.eval("g:force_pep8_conventions") == 1
    except vim.error:
        return True


def _get_parent(node, predicate):
    """bool: Find the first parent of `node` where `predicate` returns True."""
    parent = node.parent

    while parent:
        if predicate(parent):
            return parent

        parent = parent.parent


def get_corrected_code(code):
    """list[str]: Get the original code, with newlines added."""
    def _add_newlines_to_nodes(graph):
        """Force-replace parso Python nodes with objects that add newlines."""
        visitor = FlowControlVisitor("", "")
        visitor.visit(graph)

        for node in visitor.get_flow_controls():
            index = node.parent.children.index(node)
            node.parent.children[index] = NodeWrapper(node)

    graph = parso.parse(code)
    _add_newlines_to_nodes(graph)

    return [str(line) for line in graph.get_code().splitlines()]
