if &rtp !~ 'bundle/ale'
    silent !echo "add_blank_lines_for_python_control_statements_plus cannot be added because ale is not installed."

    finish
endif

if get(g:, 'vim_python_add_lines_ale_fixer_loaded', 0) == 1
    finish
endif

function! AddLinesBeforeControlStatements(buffer, lines) abort
pythonx << EOF
import line_manager
import vim

lines = vim.eval('a:lines')
lines = line_manager.get_corrected_code('\n'.join(lines))

# Important: For some reason, `ale` fixers cannot receive lines that end
# in `\`. If they do, they basically treat it as if that line + the next
# line are a single line and joins them together. To avoid this issue,
# we add a trailing whitespace. This is a hack, for now, until a better
# solution is found.
#
lines = [line + ' ' if line.endswith('\\') else line for line in lines]
vim.command('let l:corrected_lines = pyxeval("lines")')
EOF

    return l:corrected_lines
endfunction


call ale#fix#registry#Add(
\ 'add_blank_lines_for_python_control_statements_plus',
\ 'AddLinesBeforeControlStatements',
\ ['python'],
\ 'Add blank lines before control statements. But actually good.'
\ )


let g:vim_python_add_lines_ale_fixer_loaded = 1
